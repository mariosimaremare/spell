<?php

/**
* Spell is the heart of this testing framework.
*/
class Spell{
	/**
	* The directory name where the test cases are located.
	*/
	public $testDir = 'test';
	/**
	* The actual directory of the test cases.
	*/
	private $dir = null;
	
	/**
	* Retrieve the available test cases.
	* @return array of the test cases, files under the test directory marked with .in extension.
	*/
	private function testFiles(){
		$dir = getcwd() . DIRECTORY_SEPARATOR . $this->testDir . DIRECTORY_SEPARATOR;
		return(glob("{$dir}*.in"));
	}
	
	/**
	* Run the test cases and match the output.
	* @param string $executable The path of the targeted executable file.
	* @return float
	*/
	public function testAll($executable){
		$testFiles = $this->testFiles();
		$currentDir = getcwd() . DIRECTORY_SEPARATOR;
		$executable = "{$currentDir}{$executable}";
		$passed = 0;
		$nrTestFile = sizeof($testFiles);
		
		foreach($testFiles as $testFile){
			$testOutput = $currentDir . basename($testFile, ".in") . ".out";
			$expectedOutput = $currentDir . $this->testDir . DIRECTORY_SEPARATOR . basename($testFile, ".in") . ".out";
			$command = [
				"php.exe",
				"\"{$executable}\"",
				"<",
				"\"{$testFile}\"",
				">",
				"\"{$testOutput}\""
			];
			
			$strCommand = implode(" ", $command);
			
			echo("case " . basename($testFile) . ": ");
			
			system($strCommand);
			
			$outputHash = md5_file($testOutput);
			$expectedOutputHash = md5_file($expectedOutput);
			
			if($outputHash == $expectedOutputHash){
				++$passed;
				echo("passed.\n");
			} else {
				echo("failed.\n");
			}
		}
		
		echo("grade: {$passed}/{$nrTestFile}");
		return($passed/$nrTestFile);
	}
}

if(sizeof($argv) == 2){
	$spell = new Spell();
	$spell->testAll($argv[1]);
}